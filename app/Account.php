<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'accounts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'amount', 'locked',
    ];

    /**
    * Lists all account names
    * @return array
    */
    protected function all_names()
    {
        $names = [];
        foreach(self::get() as $account)
        {
            $names[$account->id] = $account->name;
        }

        return $names;
    }

    /**
    * Get entry by name
    * @param $name = string
    * @return Object
    */
    protected function by_name(string $name = '')
    {
        $res = [];
        if ($name)
        {
            $res = self::where('name',$name)->first();
        }

        return $res;
    }

    /**
    * Checks if transaction does not exceed the maximum of -100 of negative balance
    * @param $account_id = int
    * @param $amount = int
    * @return boolean
    */
    protected function can_withdraw(int $account_id = 0, int $amount = 0)
    {
        $account = self::find($account_id);

        if (count($account) && $amount)
        { // exists
            $current_amount = $account->amount;

            if (abs(($current_amount - $amount)) <= $account->overdraft)
                return true;
        }

        return false;
    }

    /**
    * Checks if account doesnt have any balance in order to close
    * @param $account_id = int
    * @return array
    */
    protected function can_close(int $account_id = 0)
    {
        $account = self::find($account_id);

        $result = false;
        $reason = 'Account doesn\'t exist';
        if(count($account)) // entry exists
        {
            $amount = $account->amount;

            if ($amount == 0) // 0 balance
            {
                if ($account->locked) // account locked
                {
                    $reason = "The account is locked. It cannot be closed";
                }
                else // can be closed
                {
                    $reason = 'Ok';
                    $result = true;
                }
            }
            else if ($amount < 0) // negative balance
            {
                $reason = 'You cannot close an account with negative balance';
            }
            else if ($amount > 0) // balance
            {
                $reason = 'You have balance. Please transfer it to another account before closing it';
            }

        }

        return ['result' => $result, 'reason' => $reason];
    }

    /**
    * Check if requested overdraft amount can be approved
    * @param $account_id = int
    * @param $overdraft = int
    * @return boolean
    */
    protected function apply_overdraft(int $account_id = 0, int $overdraft = 0)
    {
        $account = self::find($account_id);
        $res = false;
        if ($account) // entry exists
        {
            if ($overdraft <= 2000 && ($overdraft > $account->overdraft)) // overdraft cannot exceed 2000 && needs to be higher than the current overdraft
            {
                $account->overdraft = $overdraft;
                $account->save();
                $res = true;
            }
        }

        return $res;
    }
}
