<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account as Account;

class Deposit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deposit {amount=0} {account_name=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "{amount} {account_name} Deposit funds to your account. Default account_name is 'default'";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amount = $this->argument('amount');
        $name = $this->argument('account_name');
        $account = Account::by_name($name);

        if (count($account)) 
        {
            if ($amount)
            {
                $account = $account;
                $account->amount += $amount;
                $account->save();

                echo 'Thank you! Your new balance is: ' . $account->amount;
            }
            else
                echo 'Please insert a valid amount';
        }
        else
        {
            echo 'Account doesn\'t exist';
        }

        echo "\n";
    }
}
