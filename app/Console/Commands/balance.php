<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account as Account;

class Balance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'balance {account_name=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '{account_name} Shows the balance of the selected account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('account_name');
        $account = Account::by_name($name);
        
        if($account) {
            echo $name . ' account balance: ' . $account->amount;
        }
        else {
            echo 'Account doesn\'t exist';
        }
        echo "\n";

    }
}
