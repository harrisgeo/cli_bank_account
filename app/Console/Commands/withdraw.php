<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account as Account;

class Withdraw extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdraw {amount=0} {account_name=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "{amount} {account_name} Withdraw funds from your account. Default account_name is 'default";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amount = (int) $this->argument('amount');
        $name = $this->argument('account_name');
        $account = Account::by_name($name);

        if (count($account)) // account exists
        {
            if (Account::can_withdraw($account->id, $amount))
            { // does not exceed the maximum of -100 of negative balance
                $account->amount -= $amount;
                $account->save();

                echo 'Please take your cash. Your new balance is: ' . $account->amount;
            }
            else
            { // transaction exceeds -100
                echo 'Transaction failed. Please check that your account has the sufficient funds required for this transaction';
            }
            
        }
        else // account doesnt exist
        {
            echo 'Account doesn\'t exist';
        }

        echo "\n";
    }
}
